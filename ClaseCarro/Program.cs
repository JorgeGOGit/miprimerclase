﻿using System;
//Nombre del proyecto
namespace ClaseCarro
{
    class Program
    {
        //static = global 
        //void = sin regreso de valores
        static void Main(string[] args)
        {
         // Instanciar            
         Carro CarroNuevo = new Carro();             
         CarroNuevo.carrofamiliar();
         CarroNuevo.carrodeportivo();
         CarroNuevo.imprime();    
         //CarroNuevo.inicializavar();
         CarroNuevo.leedatos();                         
         CarroNuevo.imprime();    
        }
    }
   public class Carro 
    {
       //Atributos        
        public string Marca = "Honda CRV"; 
        public string Modelo = "2018"; 
        public string Color= "Gris"; 
        public string Kilometraje = "5,000"; 
        public string Precio = "225,000"; 
        public string tipo = "Seminuevo";   
        public int nVeces = 1;

        //Metodos        
        public void carrofamiliar()
        {
         Console.WriteLine("Carro Familiar ");     
         Console.WriteLine("Caracteristicas a considerar : ");  
         Console.WriteLine("1. Seguridad en caso de accidente.");  
         Console.WriteLine("2. Ahorro de combustible. ");  
         Console.WriteLine("3. Bajo coste de mantenimiento.");  
         Console.WriteLine("4. Fiabilidad. ");  
         Console.WriteLine("5. Comodidad. ");  
         Console.WriteLine("6. Calidad del acabado. ");  
         Console.WriteLine("7. Espacioso / Amplio. ");  
         Console.WriteLine("8. Respetuoso con el medio ambiente.");  
         Console.WriteLine("9. Conducción tranquila / silenciosa. ");  
         Console.WriteLine("10. Agradable / divertido de conducir. ");  
         Console.WriteLine("Marca= HONDA Modelo=2015  Color=TODOS Kilometraje=5000  Precio=225,000  Tipo= Nuevo,seminuevo " ); 
         Console.WriteLine(" ");  
         Console.WriteLine("Oprima cualquier tecla para continuar");  
         Console.ReadKey();
        }
        public void carrodeportivo()
        {
         Console.WriteLine("Carro Deportivo ");     
         Console.WriteLine("Caracteristicas a considerar : ");  
         Console.WriteLine("1. Seguridad en caso de accidente.");  
         Console.WriteLine("2. Alto consumo de combustible. ");  
         Console.WriteLine("3. Alto  coste de mantenimiento.");  
         Console.WriteLine("3. Alto  desempeño.");  
         Console.WriteLine("4. Fiabilidad. ");  
         Console.WriteLine("5. Comodidad. ");            
         Console.WriteLine("6. Poco Espacioso. ");                                
         Console.WriteLine("Marca= Porche Modelo=1963  Color=Gris Kilometraje=5000  Precio=2,325,000  Tipo= Nuevo,seminuevo " );   
         Console.WriteLine(" ");  
         Console.WriteLine("Oprima cualquier tecla para continuar");  
         Console.ReadKey();
        }
        public void inicializavar()
        {
         Console.WriteLine("Inicializando Variables");  
         Marca = ""; Modelo = ""; Color = ""; Kilometraje = ""; Precio = ""; tipo = ""; nVeces = 1;              
         Console.WriteLine("Marca= "+Marca + " Modelo= " +Modelo+ "Color= " +Color+ " Kilometraje= " +Kilometraje+ " Precio= " +Precio+ " Tipo= " +tipo ); 
         Console.WriteLine("Termino Inicializando Variables");                     
         Console.WriteLine(" ");
         Console.WriteLine(" ");  
         Console.WriteLine("Oprima cualquier tecla para continuar");  
         Console.ReadKey();
        }  
        public void leedatos()
        {          	      
         Console.WriteLine(" ");
         Console.WriteLine("Captura de datos del carro:"); 
         Console.WriteLine("Ingrese Marca:");
         this.Marca = Console.ReadLine();             
         Console.WriteLine("Ingrese Modelo:");
         this.Modelo = Console.ReadLine();     
         Console.WriteLine("Ingrese Color:");
         this.Color = Console.ReadLine();     
         Console.WriteLine("Ingrese Kilometraje:");
         this.Kilometraje = Console.ReadLine();     
         Console.WriteLine("Ingrese Precio:");
         this.Precio = Console.ReadLine();     
         Console.WriteLine("Ingrese Tipo:");
         this.tipo = Console.ReadLine();                   
        }
        public void imprime() 
        {
         Console.WriteLine(""); 
         Console.WriteLine("IMPRIME D A T O S   D E L   C A R R O:");              
         Console.Write("Marca       :");
         Console.WriteLine(Marca);
         Console.Write("Modelo      :");
         Console.WriteLine(Modelo);
         Console.Write("Color       :");
         Console.WriteLine(Color);
         Console.Write("kilometraje :");
         Console.WriteLine(Kilometraje);
         Console.Write("Precio      :");
         Console.WriteLine(Precio);
         Console.Write("Tipo        :");
         Console.WriteLine(tipo);                         
        }         
    }
}
